#include<stdio.h>
#include<windows.h>
#include<conio.h>
#include<list>
using namespace std;

#define UP 72
#define LEFT 75
#define RIGHT 77
#define DOWN 80



void gotoxy(int x, int y){
    HANDLE hCon;
    hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD dwPos;
    dwPos.X = x;
    dwPos.Y = y;
    SetConsoleCursorPosition(hCon, dwPos);
}
void HideCursor(){
    HANDLE hCon;
    hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO cci;
    cci.dwSize = 2;
    cci.bVisible = false;

    SetConsoleCursorInfo(hCon, &cci);
}

void Draw_limits(){
    for(int i = 2 ; i < 78; i++)
    {
        gotoxy(i, 3); printf("%c",205);
        gotoxy(i, 33); printf("%c",205);
    }
    for (int i = 4 ; i < 33 ; i++)
    {
        gotoxy(2, i); printf("%c", 186);
        gotoxy(77, i); printf("%c", 186);
    }
    gotoxy(2, 3); printf("%c", 201);
    gotoxy(2, 33); printf("%c", 200);
    gotoxy(77, 3);printf("%c", 187);
    gotoxy(77, 33);printf("%c", 188);
}
class SpaceShip{
    int x, y;
    int hearts;
    int lifes;

public:
    SpaceShip(int _x, int _y, int _hearts, int _lifes): x(_x), y(_y), hearts(_hearts), lifes(_lifes){}
    int getx(){ return x; }
    int gety(){ return y; }
    void MinusHeart(){
        hearts--;
    }
    void Draw();
    void Erase();
    void Move();
    void Draw_Hearts();
    void Die();
    int getlifes(){ return lifes;}
};

class Asteroid{
    int x, y;
public:
    Asteroid(int _x, int _y): x(_x), y(_y){}
    void Draw();
    void Move();
    void Collision(class SpaceShip &SS);
    int getx(){return x;}
    int gety(){return y;}
};

class Projectile{
    int x, y;
public:
    Projectile(int _x, int _y): x(_x), y(_y){}
    int getx(){ return x;}
    int gety(){ return y;}
    void Trajectory();
    bool Out();
};
bool Projectile::Out(){
    if (y == 4){
        return true;
    }
    return false;

}
void Projectile::Trajectory(){
        gotoxy(x, y); printf(" ");
        y--;
        gotoxy(x, y); printf("*");

}

void SpaceShip::Draw(){

    gotoxy(x, y);   printf("  %c  ", 30);
    gotoxy(x, y+1); printf(" %c%c%c ", 40, 207, 41);
    gotoxy(x, y+2); printf("%c%c %c%c", 30,190,190,30);
}
void SpaceShip::Erase(){
    gotoxy(x, y);   printf("        ");
    gotoxy(x, y+1); printf("        ");
    gotoxy(x, y+2); printf("        ");
}
void SpaceShip::Move(){
    if(kbhit()){
            char key = getch();

            Erase();
            if (key == LEFT && x > 3){
                x--;
            }
            if (key == RIGHT && x + 5 < 77){
                x++;
            }
            if (key == UP && y > 4){
                y--;
            }
            if (key == DOWN && y + 3 < 33){
                y++;
            }
            Draw();
            Draw_Hearts();
        }
}
void SpaceShip::Draw_Hearts(){

    gotoxy(50, 2); printf("Lifes %d", lifes);
    gotoxy(64, 2); printf("Health");
    gotoxy(70, 2); printf("       ");
    for(int i = 0 ; i < hearts; i++)
    {
        gotoxy(70 + i, 2); printf("%c", 3);
    }

}
void SpaceShip::Die(){
    if (hearts == 0){
        Erase();
        gotoxy(x, y);     printf("   **   ");
        gotoxy(x, y + 1); printf("  ****  ");
        gotoxy(x, y + 2); printf("   **   ");
        Sleep(200);

        Erase();
        gotoxy(x, y);     printf(" * ** *");
        gotoxy(x, y + 1); printf("  **** ");
        gotoxy(x, y + 2); printf(" * ** *");
        Sleep(200);
        Erase();
        lifes--;
        hearts = 3;
        Draw_Hearts();
        Draw();

    }
}



void Asteroid::Draw(){
    gotoxy(x, y); printf("%c", 184);
}

void Asteroid::Move(){
    gotoxy(x, y); printf(" ");
    y++;
    if( y > 32){
        x = rand()%71 + 4;
        y = 4;
    }
    Draw();
}

void Asteroid::Collision(SpaceShip &SS){
    if ( x >= SS.getx() && x < SS.getx() + 6 && y >= SS.gety() && y <= SS.gety() + 2)
    {
        SS.MinusHeart();
        SS.Erase();
        SS.Draw();
        SS.Draw_Hearts();
        x = rand()%71 + 4;
        y = 4;
    }
}


int main(){

    list<Projectile*> P;
    list<Projectile*>::iterator it;

    list <Asteroid*> A;
    list <Asteroid*>::iterator itA;
    Draw_limits();
    HideCursor();
    SpaceShip SS(37, 30, 3, 5);
    SS.Draw();
    SS.Draw_Hearts();
    int score = 0;


    for(int i = 0; i < 7; i++)
    {
        A.push_back(new Asteroid(rand() % 75 + 3, rand() % 5 + 4));
    }
    bool gameover = false;


    while (!gameover){
            gotoxy(4, 2); printf("Score %d", score);
            if (kbhit()){
                char key = getch();
                if(key == 's'){
                    P.push_back(new Projectile(SS.getx() + 2, SS.gety() - 1));
                }
            }
            for (it = P.begin(); it != P.end(); it++)
            {
                (*it)->Trajectory();
                if((*it)->Out()){
                    gotoxy((*it)->getx(), (*it)->gety()); printf(" ");
                    delete(*it);
                    it = P.erase(it);
                }

            }

            for (itA = A.begin(); itA != A.end(); itA++)
            {
                (*itA)->Move();
                (*itA)->Collision(SS);
            }
            for(itA = A.begin(); itA != A.end(); itA++)
            {
                for(it = P.begin(); it != P.end(); it++)
                {
                    if((*itA)->getx() == (*it)->getx() && ((*itA)->gety() + 1 == (*it)->gety() || (*itA)->gety() == (*it)->gety()))
                    {
                        gotoxy((*it)->getx(), (*it)->gety()); printf(" ");
                        delete(*it);
                        it = P.erase(it);
                        A.push_back(new Asteroid(rand()%74 + 3 , 4));
                        gotoxy((*itA)->getx(), (*itA)->gety()); printf(" ");
                        delete(*itA);
                        itA = A.erase(itA);

                        score += 5;
                    }
                }
            }
            if(SS.getlifes() == 0){
                gameover = true;
            }
            SS.Die();
            SS.Move();
            Sleep(30);
    }
    return 0;
}

